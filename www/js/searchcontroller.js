'use strict';

var searchCtrl = function($scope,$location, $stateParams, $ionicModal, Productfactory){
	$scope.searchresults = {};
	$scope.searchkeyword = $stateParams.searchkeyword;
  
 	Productfactory.searchProduct($scope.searchkeyword, function(data){
 		$scope.searchresults = data;
 	});

 	$scope.backBtn = function() {
		window.history.back();
	};
 	
 	$scope.goToItem = function(item) {
		$location.path('/Item/' + item.ItemCode);
 	};

};

Application.Controllers.controller('searchCtrl',['$scope','$location', '$stateParams', '$ionicModal', 'Productfactory', searchCtrl]);