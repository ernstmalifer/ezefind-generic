'use strict';

// var SOCKET_HOST = 'http://localhost:3000';

var Productfactory = function($http, _){
	var productfactory = {};

	productfactory.allitems = [];
	productfactory.compareItems = [];
	productfactory.wishlistItems = [];
	productfactory.tempcwishlist = [];

	productfactory.getAll = function(callback){
		$http.get('data.json').success(function(data){
			productfactory.allitems = data;
			// productfactory.allitems = ParseJsonFactory.parseJsonAttributes(data);
			_.each(productfactory.allitems, function(eachitem){ eachitem.inCompare = false; eachitem.inWishlist = false; })
			console.log(productfactory.allitems);
		});	
	}

	productfactory.getPhones = function(catname,callback){
		callback(_.where(productfactory.allitems, {SubChildCategory: catname}));
	}

	productfactory.getParentCategory = function(callback){
		var count = _.countBy(productfactory.allitems, function(obj){
			return obj.ParentCategory;
		})
		callback(count);
	}

	productfactory.getChildCategory = function(item,callback){
		var count = _.countBy(_.where(productfactory.allitems, {ParentCategory: item}),function(obj){
			return obj.ChildCategory;
		});
		callback(count);
	}

	productfactory.getSubChildCategory = function(item,callback){
		var count = _.countBy(_.where(productfactory.allitems, {ChildCategory: item}),function(obj){
			return obj.SubChildCategory;
	});
		callback(count);
	}

	productfactory.getalldata = function(callback){
		callback(productfactory.allitems);
	}

	productfactory.getComparetoAdd = function(callback){
		callback(_.difference(productfactory.allitems,productfactory.compareItems));
	}

	productfactory.getAllItemsToCompare = function(callback){
		callback(productfactory.allitems);
	}

	productfactory.getalldataChild = function(childdata,callback){
		callback(_.where(productfactory.allitems,{ChildCategory: childdata}));
	}

	productfactory.getPhoneItem = function(itemid,callback){
		var phoneitem = _.find(productfactory.allitems, function(dataitem){ return dataitem.ItemCode == itemid });
		callback(phoneitem);
	};

	productfactory.getSubPhone = function(child,callback){
		callback(_.where(productfactory.allitems, {ParentCategory: child}));
	};
	
	productfactory.sorting = function(catname,callback){
		callback(_.where(productfactory.allitems, {SubChildCategory: catname}));
	};

   
	productfactory.addItemToCompare = function(item){
		var itemaddedtocompare = _.find(productfactory.allitems, function(dataitem){ return dataitem.ItemCode == item.ItemCode; });
		var itemaddedtocompareall = _.filter(productfactory.allitems, function(dataitem){ return dataitem.inCompare; });
		
		if(itemaddedtocompareall.length < 3){
			if(!itemaddedtocompare.inCompare){ //not in compare
				productfactory.compareItems.push(item);
				itemaddedtocompare.inCompare = true;
				itemaddedtocompareall = _.filter(productfactory.allitems, function(dataitem){ return dataitem.inCompare });
				console.log('Add compare',itemaddedtocompareall.length);
			}else{
				console.log('same data');
			}
			console.log('execute');
		}
		
		console.log('Add compare',itemaddedtocompareall.length);
	};

	productfactory.addItemToWishlist = function(item){
		var itemaddedtowishlist = _.find(productfactory.allitems, function(dataitem){ return dataitem.ItemCode == item.ItemCode;   });
		var itemaddedtowishlistall = _.filter(productfactory.allitems, function(dataitem){ return dataitem.inCompare; });
		itemaddedtowishlist.inWishlist = true;
		console.log('add wishlist',itemaddedtowishlistall.length);
		productfactory.wishlistItems.push(item);
	};

	productfactory.removeItemToCompare = function(item,index){
		var itemaddedtocompare = _.find(productfactory.allitems, function(dataitem){ return dataitem.ItemCode == item.ItemCode });
		console.log(itemaddedtocompare.length);
		if(itemaddedtocompare.inCompare){ //in compare
			console.log(item);
			
			itemaddedtocompare.inCompare = false;
			productfactory.compareItems.splice(index,1);
		}
		console.log('Remove compare',itemaddedtocompare.length)
	};

	productfactory.removeItemToWishlist = function(item,index){
		var itemremovetowishlist = _.find(productfactory.allitems, function(dataitem){ return dataitem.inWishlist;});
		console.log(item, itemremovetowishlist);
		if(itemremovetowishlist.inWishlist){ //in compare

			itemremovetowishlist.inWishlist = false;
			productfactory.wishlistItems.splice(index,1);
		}
		console.log('remove wishlist',itemremovetowishlist.length);
	};

	productfactory.inCompare = function(id, callback){
		var inCompare = _.find(productfactory.allitems, function(dataitem){ if(dataitem.ItemCode == id){ return dataitem.inCompare} });
		callback(inCompare);
	};

	productfactory.getCompareItems = function(callback){
		callback(productfactory.compareItems);
	};

	productfactory.inWishlist = function(id, callback){
		var inWishlist = _.find(productfactory.allitems, function(dataitem){ if(dataitem.ItemCode == id){ return dataitem.inWishlist; } });
		callback(inWishlist);
	};

	productfactory.getWishlistItems = function(callback){
		callback(productfactory.wishlistItems);
	};

	productfactory.searchProduct = function(keyword, callback){
	var data = _.filter(productfactory.allitems, function(data){
		var re = new RegExp(keyword,"ig");
		return data.Name.match(re);
	})
	callback(data);
	};

	productfactory.clearCompareItems = function () {
		productfactory.compareItems.length = 0;
	};

	productfactory.getSubPhonetoFilter = function(type,subname,data,callback){
		if(type == 'brand'){
			callback(_.filter(productfactory.allitems, function(item){
			if(item.brand == data && item.subcategory == subname)
				return true;
			}));
		}else if(type == 'Dimensions'){
			callback(_.filter(productfactory.allitems, function(item){
			if(item.dimensions == data && item.subcategory == subname)
				return true;
			}));
		}else{
			callback(_.filter(productfactory.allitems, function(item){
			if(item.color == data && item.subcategory == subname)
				return true;
			}));
		}
	};

	productfactory.Filtering = function(type,category,brand,callback){
		if(type == 'brand'){
		callback(_.filter(productfactory.allitems,function(dataitem){
			if(dataitem.Display == brand) 
				return true;
		}));

		}else{
			callback(_.filter(productfactory.allitems,function(dataitem){
			if(dataitem.Resolution == brand) 
				return true;
		}));
		}
	};

	return productfactory;

};


var underscore = function () {
   return window._;
};

// var SocketFactory = function () {
// 	// return io.connect(SOCKET_HOST);
// 	var socket = io.connect('http://localhost:3300', {transports: ['websocket']});
// 	return socket
// }

Application.Services.factory('_', [underscore]);
// Application.Services.factory('SocketFactory', [SocketFactory]);
Application.Services.factory('Productfactory',['$http', '_', Productfactory]);