'use strict';

var phoneitemCtrl = function($scope,$location, $stateParams, $ionicModal, $timeout, Productfactory, _, $ionicScrollDelegate){
	$scope.phoneitem = {};
	$scope.quantity = 0;
	$scope.phonerelateditems = [];

 	Productfactory.getPhoneItem($stateParams.phoneid, function(data){
 		$scope.phoneitem = data;
 		$scope.mainimage = $scope.phoneitem.images[0];
 		$scope.models = $scope.phoneitem.model[0];
 		console.log($scope.phoneitem);
 	});

 	Productfactory.inCompare($stateParams.phoneid, function(data){
 		$scope.inCompare = data;
 	});

 	Productfactory.inWishlist($stateParams.phoneid, function(data){
 		$scope.inWishlist = data;
 	});

 	Productfactory.getPhones($scope.phoneitem.SubChildCategory, function(data){
 		$scope.phonerelateditems = _.filter(data, function(dataitem){ return dataitem.ItemCode != $stateParams.phoneid; });;
 	});

	$scope.scrollLeft = function() {
		$ionicScrollDelegate.$getByHandle('list').scrollBy(-400,0, true);
	};

	$scope.scrollRight = function() {
		// $scope.data = {allowScroll: true}
		// if($scope.phonerelateditems.length <=4){
		// 	$scope.data.allowScroll = !$scope.data.allowScroll;
		// }else{
		// 	$ionicScrollDelegate.$getByHandle('list').scrollBy(400,0, true);
		// }
		$ionicScrollDelegate.$getByHandle('list').scrollBy(400,0, true);
	};

	$scope.addItemToCompare = function(item) {
	  Productfactory.getCompareItems(function(data){
	   $scope.collect = data;
	   if($scope.collect.length !== 4){
	    $scope.inCompare = true;
	    $scope.button = false;
	    Productfactory.addItemToCompare(item);
	    $scope.removeitem = 'Successfully Added To Compare List';
	    $scope.openCompareModal();
	   }
	  });
	 }

 	$scope.setMainImage = function(image) {
		$scope.mainimage = image;
 	};

 	$scope.minus = function() {
 		if($scope.quantity > 0) {
 			--$scope.quantity;
 		}
 	}
 	$scope.add = function() {
		++$scope.quantity;
 	}

	$ionicModal.fromTemplateUrl('template/modal/comparemodal.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.comparemodal = modal;
	});

	$scope.openCompareModal = function() {
		$scope.comparemodal.show();
		$timeout(function(){ $scope.closeCompareModal(); }, 2000)
	};

	$scope.closeCompareModal = function() {
		$scope.comparemodal.hide();
	};

	$ionicModal.fromTemplateUrl('template/modal/relateditemModal.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.relatedmodal = modal;
	});

	$scope.openRelatedModal = function() {
		$scope.relatedmodal.show();
	};

	$scope.closeRelatedModal = function() {
		$scope.relatedmodal.hide();
	};

	$ionicModal.fromTemplateUrl('template/modal/wishlistmodal.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.wishlistmodal = modal;
	});

	$scope.openWishlistModal = function() {
		$scope.wishlistmodal.show();
		$timeout(function(){ $scope.closeWishlistModal(); }, 2000)
	};

	$scope.closeWishlistModal = function() {
		$scope.wishlistmodal.hide();
	};


	//Cleanup the modal when we're done with it!
	$scope.$on('$destroy', function() {
		$scope.comparemodal.remove();
	});

	// Execute action on hide modal
	$scope.$on('modal.hidden', function() {
	// Execute action
	});

	// Execute action on remove modal
	$scope.$on('modal.removed', function() {
	// Execute action
	});

	$scope.goToPhone = function(phoneid){
 		$location.path('/Item/' + phoneid); 
 	}

	$scope.removeItemToCompare = function(item) {
		$scope.inCompare = false;
		$scope.button = false;
		Productfactory.removeItemToCompare(item.id);
	}

	$scope.closeModalAndProceedToCompare = function(){
		$scope.closeCompareModal();
		$location.path('/compare')
	}

	$scope.addItemTowishlist = function(item) {
		$scope.inWishlist = true;
		$scope.button = false;
		Productfactory.addItemToWishlist(item);
		$scope.removeitem = 'Successfully Added To WishList';
		$scope.openWishlistModal();
	}

	$scope.removeItemToWishlist = function(item) {
		$scope.inWishlist = false;
		Productfactory.removeItemToWishlist(item.id);
	}

	$scope.closeModalAndProceedToWishlist = function(){
		$scope.closeWishlistModal();
		$location.path('/wishlist')
	}

	$ionicModal.fromTemplateUrl('template/modal/sharemodal.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.sharemodal = modal;
	});

	$scope.showShareModal = function() {
		$scope.sharemodal.show();
	};

	$ionicModal.fromTemplateUrl('template/modal/shareemailmodal.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.shareemailmodal = modal;
	});

	$scope.shareViaEmail = function() {
		$scope.sharemodal.hide();
		$scope.shareemailmodal.show();
	};

	$scope.keyboard = function () { 
   		$('#shareemail').keyboard();
	};

	$scope.shareAndClose = function() {
		$scope.shareemailmodal.hide();
	};

};

Application.Controllers.controller('phoneitemCtrl',['$scope','$location', '$stateParams', '$ionicModal', '$timeout', 'Productfactory', '_','$ionicScrollDelegate', phoneitemCtrl]);