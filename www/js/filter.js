'use strict';


var duplicates = function(){
    return function (arr, field) {
        return _.uniq(arr, function(a) { return a[field]; });
	};
}

var newline = function(){
	return function (text){
		if(!(text == undefined || text == null)){
			return text.replace(/\n/g, '<br/>');
		}else{
			return 'Nothing to display.';
		}
	};
}

// var placeHolder = function(data){
// 	var me = data;
// 	return function (myArr){
// 		if(myArr.length == 0){
// 			return 
// 		}
// 	}
// }

Application.Filters.filter('duplicates', duplicates);
Application.Filters.filter('newline', newline);
// Application.Filters.filter('placeHolder', placeHolder);
