'use strict';

var compareCtrl = function($scope, $location,$ionicModal,$timeout,Productfactory, $ionicScrollDelegate, $rootScope){
	$scope.compareitems = {};
	$scope.allITems = [];
	// $scope.itemAdded = true;
  
	Productfactory.getCompareItems(function(data){
 		$scope.compareitems = data;
 	});

 	$ionicModal.fromTemplateUrl('template/modal/allitemModal.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.allitemModal = modal;
	});

	$scope.goToPhone = function(phoneid){
 		$location.path('/Item/' + phoneid); 
 	}

	$scope.closeallitemModal = function() {
		$scope.allitemModal.hide();
	};

	$scope.openallitemModal = function() {
		Productfactory.getComparetoAdd(function(data){
 		$scope.allITems = data;
	 	});
		$scope.allitemModal.show();
	};

	$scope.scrollLeft = function() {
		$ionicScrollDelegate.$getByHandle('list').scrollBy(-500,0, true);
	};

	$scope.scrollRight = function() {
		$ionicScrollDelegate.$getByHandle('list').scrollBy(500,0, true);
	};

	$ionicModal.fromTemplateUrl('template/modal/comparemodal.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.comparemodal = modal;
	});

	$scope.openCompareModal = function() {
		$scope.comparemodal.show();
	};

	$scope.closeCompareModal = function() {
		$scope.comparemodal.hide();
	};

	$scope.removeItemToCompare = function(item, otheritem,index) {
		$scope.inCompare = false;
		$scope.button = true;
		$scope.removeitem = 'Are you sure you want to remove "' +otheritem.Name+ '" in Compare List';
		$scope.openCompareModal();
		
		$scope.alert = function(data){
			if(data == 'Yes'){
				Productfactory.removeItemToCompare(otheritem, index);
				Productfactory.getCompareItems(function(data){
				$scope.compareitems = data;
				$scope.closeCompareModal();
				});
			}else{
				$scope.closeCompareModal();
			}
		}
	}

	$scope.addItemToCompare = function(item) {
	   Productfactory.getCompareItems(function(data){
	    $scope.collect = data;
	    if($scope.collect.length !== 3){
		    $scope.removeitem = 'Successfully Added To Compare List';
			$scope.inCompare = true;
			// $rootScope.itemAdded = true;
			Productfactory.addItemToCompare(item);
			if($scope.collect.length == 3){
				$rootScope.itemAdded = true;
			}else{
				$rootScope.itemAdded = false;
			}
	    }
	   });
	}

};

Application.Controllers.controller('compareCtrl',['$scope','$location','$ionicModal','$timeout','Productfactory', '$ionicScrollDelegate', '$rootScope', compareCtrl]);