'use strict';

var temp = '';
var temp1 = '';
var layout = 'grid';

var mainCtrl  = function($scope,$ionicModal,$location,$ionicSlideBoxDelegate,Productfactory,_,$http, limitToFilter, $ionicPopover){
		$scope.selected = undefined;
		$scope.temp = function(data1){
			temp = data1;
		}

		$scope.temp1 = function(data){
			temp1 = data;
		}

		$scope.keyboard = function () { 

			if(!$scope.showsearch) {
       			$('#ipad').getkeyboard().reveal(true);
			} else {
				$('#ipad').getkeyboard().close();
			}
    	};

    	$scope.nextSlide = function(data) {
    		if(data =='previous'){
    			$ionicSlideBoxDelegate.previous();
    		}
    		else{
    			$ionicSlideBoxDelegate.next();
    			}
  			}
    	$scope.clear = function(length){
    		if(length){
    			$('#ipad').val($('#ipad').val().slice(0, - 1));
    		} else {
    			$('#ipad').val('');
    		}
    		// $('#ipad').getkeyboard().reveal(true);
    	}
  
	    $scope.change = function(searchkeyword) {
	    	console.log(searchkeyword);
	    };

	    $scope.search = function(){
	      $scope.search.searchkeyword;
	      $location.path('/search/' + $scope.search.searchkeyword) //bug if keyboard is typed, returns undefined
	      $scope.search.searchkeyword = $('#ipad').val();
	      $location.path('/search/' + $scope.search.searchkeyword );
	     };

	    var mySwiper = new Swiper('.swiper-container',{
		    slidesPerView:3,
		    loop:true,
		    autoplayDisableOnInteraction: true,
		    tdFlow: {
		      rotate : 15,
		      stretch :-86,
		      depth: 200,
		      modifier : 3,
		      shadows:false
		    }
	 	});

	 	$('.arrow-left').on('click', function(e){
		    e.preventDefault()
		    mySwiper.swipePrev()
	    })
		$('.arrow-right').on('click', function(e){
		    e.preventDefault()
		    mySwiper.swipeNext()
	    })

	    $scope.backBtn = function() {
			window.history.back();
		};

		$ionicModal.fromTemplateUrl('template/modal/sortmodal.html', {
			scope: $scope,
			animation: 'slide-in-up'
		}).then(function(modal) {
			$scope.sortmodal = modal;
		});

		$ionicModal.fromTemplateUrl('template/modal/parent-modal.html', {
			scope: $scope,
			animation: 'slide-in-up'
		}).then(function(modal) {
			$scope.shopbycategoryModal = modal;
		});

		$ionicModal.fromTemplateUrl('template/modal/filtermodal.html', {
			scope: $scope,
			animation: 'slide-in-up'
		}).then(function(modal) {
			$scope.filtermodal = modal;
		});

		$ionicModal.fromTemplateUrl('template/modal/child-modal.html', {
			scope: $scope,
			animation: 'slide-in-up'
		}).then(function(modal) {
			$scope.subcategoryModalphone = modal;
		});

		$ionicModal.fromTemplateUrl('template/modal/subchild-modal.html', {
			scope: $scope,
			animation: 'slide-in-up'
		}).then(function(modal) {
			$scope.subcategoryModalcamera = modal;
		});

		$scope.openshopbycategoryModal = function(item) {
			$scope.shopbycategoryModal.show();

			Productfactory.getParentCategory(function(data){
				$scope.Parent = data;
			});
		};

		$scope.backBtn = function() {
			window.history.back();
		};

		$scope.opensortModal = function() {
			$scope.sortmodal.show();
		};

		$scope.closesortModal = function() {
			$scope.sortmodal.hide();
		};

		$scope.closeshopbycategoryModal = function() {
			$scope.shopbycategoryModal.hide();
		};

		$scope.openfiltermodal = function() {
			$scope.filtermodal.show();

			Productfactory.getalldata(function(data){
				$scope.filter = data;
				console.log($scope.filter);
			});
		};

		$scope.closefiltermodal = function() {
			$scope.filtermodal.hide();
		};

		$scope.opensubcategoryModalphone = function(item) {
			$scope.subcategoryModalphone.show();

			Productfactory.getChildCategory(item, function(data){
				$scope.Child = data;
			});
		};

		$scope.closesubcategoryModalphone = function() {
	        $scope.subcategoryModalphone.hide();
	        $scope.shopbycategoryModal.hide();
		};

		$scope.opensubcategoryModalcamera = function(item) {
			$scope.subcategoryModalcamera.show();
			Productfactory.getSubChildCategory(item, function(data){
				$scope.SubChild = data;
			});
		};

		$scope.closesubcategoryModalcamera = function() {
			$scope.subcategoryModalcamera.hide();
	        $scope.subcategoryModalphone.hide();
	        $scope.shopbycategoryModal.hide();
		};
};

var phoneCtrl = function($scope,$ionicModal,$stateParams, $location,$filter,$timeout, Productfactory, $ionicScrollDelegate){

	$scope.layout = layout;
	var listscroll;
	$scope.scrollabletop = false;
	$scope.scrollablebottom = true;

	$scope.temp = function(data){
		temp = data;
	}

	$scope.orientation = function(data){
		layout = data;
	}

	Productfactory.inCompare($stateParams.phoneid, function(data){
		$scope.inCompare = data;
	});

 	Productfactory.inWishlist($stateParams.phoneid, function(data){
 		$scope.inWishlist = data;
 	});

 	Productfactory.getPhones($stateParams.categorytype,function(data){
 		$scope.phones = data;
 		// if($scope.phones.length == 0){
 		// 	$scope.phones = 'Nothing to Display'
 		// }else {
 		// 	$scope.phones = data;
 		// }
 		console.log($scope.phones, $scope.phones.length);
 	});

 	Productfactory.getSubPhone($stateParams.subtype,function(data){
		$scope.subphones = data;
	});

 	$scope.goToPhone = function(phoneid){
 		$location.path('/Item/' + phoneid); 
 	}

 	$ionicModal.fromTemplateUrl('template/modal/wishlistmodal.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.wishlistmodal = modal;
	});

	$scope.openWishlistModal = function() {
		$scope.wishlistmodal.show();
		$timeout(function(){ $scope.closeWishlistModal(); }, 2000)
	};

	$scope.closeWishlistModal = function() {
		$scope.wishlistmodal.hide();
	};

	$ionicModal.fromTemplateUrl('template/modal/comparemodal.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.comparemodal = modal;
	});

	$scope.openCompareModal = function() {
		$scope.comparemodal.show();
		$timeout(function(){ $scope.closeCompareModal(); }, 2000)
	};

	$scope.closeCompareModal = function() {
		$scope.comparemodal.hide();
	};

	$scope.inWishlist = {};
	$scope.inCompare = {};

	$scope.addItemTowishlist = function(item) {
		Productfactory.addItemToWishlist(item);
			$scope.removeitem = 'Successfully Added To WishList';
			$scope.openWishlistModal();
	};

	$scope.addItemToCompare = function(item) {
		Productfactory.getCompareItems(function(data){
	    $scope.collect = data;
	    if($scope.collect.length !== 3){
		    $scope.removeitem = 'Successfully Added To Compare List';
			$scope.inCompare = true;
			Productfactory.addItemToCompare(item);
			$scope.openCompareModal();
	    }
	   });

		// Productfactory.addItemToCompare(item);
		// 	$scope.removeitem = 'Successfully Added To Compare List';
		// Productfactory.getCompareItems(function(data){
		// 	$scope.compareitems = data;
		// 	console.log($scope.compareitems.length);
		// });

	// $scope.addItemTowishlist = function(item,index) {
	// 	Productfactory.addItemToWishlist(item);
	// 	$scope.specicInWishlist[index] = true;
	// 	$scope.removeitem = 'Successfully Added To WishList';
	// 	$scope.openWishlistModal();
	// }

	// $scope.addItemTowishlist = function(item) {
	// 	// console.log('added');
	// 	// $scope.item.inWishlist = true;
	// 	// $scope.button = false;
	// 	// Productfactory.addItemToWishlist(item);
	// 	Productfactory.addItemToWishlist(item);
	// 	$scope.removeitem = 'Successfully Added To WishList';
	// 	$scope.openWishlistModal();
	// }

	// $scope.addItemToCompare = function(item, index) {
		
	// 	   Productfactory.getCompareItems(function(data){
	// 	    $scope.collect = data;
	// 	    if($scope.collect.length !== 4){
	// 	     	Productfactory.addItemToCompare(item);
	// 			$scope.specificinCompare[index] = true;
	// 			$scope.removeitem = 'Successfully Added To Compare List';
	// 			$scope.openCompareModal();
	// 	    }
	// 	   });
	  // Productfactory.addItemToCompare(item);
	  //    $scope.removeitem = 'Successfully Added To Compare List';
	  //    $scope.openCompareModal();
	  //    Productfactory.getCompareItems(function(data){
	  //  $scope.compareitems = data;
	  //  console.log($scope.compareitems.length);
	  // });

	  // }

	// $scope.addItemToCompare = function(item) {
	//   // Productfactory.getCompareItems(function(data){
	//   //  $scope.collect = data;
	//   //  if($scope.collect.length !== 4){
	//   //   $scope.inCompare = true;
	//   //   $scope.button = false;
	//   //   Productfactory.addItemToCompare(item);
	//   //  }
	//   // });

		// Productfactory.addItemToCompare(item);
	 //    $scope.removeitem = 'Successfully Added To Compare List';
	 //    $scope.openCompareModal();
	 }

	$scope.sortnow = function(sortdata){
			Productfactory.sorting(temp,function(data){
				$scope.phones = $filter('orderBy')(data,sortdata);
			}); 
	}

	$scope.filternow = function(type,brand){
			Productfactory.Filtering(type,temp,brand,function(data){
				$scope.phones = data;
			});
	}

	$scope.changelayout = function(type) {
		if(type == 'grid') {
			$ionicScrollDelegate.$getByHandle('grid').scrollTop();
		} else {
			$ionicScrollDelegate.$getByHandle('list').scrollTop();
		}

		$scope.scrollabletop = false;
		$scope.scrollablebottom = true;
	}

	$scope.scrollTop = function() {

		if($scope.layout == 'grid'){
			listscroll = $ionicScrollDelegate.$getByHandle('grid');
		} else {
			listscroll = $ionicScrollDelegate.$getByHandle('list');
		}
		
			listscroll.scrollBy(0,-300, true);
		// if(listscroll.getScrollPosition().top){
		// 	$scope.scrollablebottom = true;
		// } else {
		// 	$scope.scrollabletop = false;
		// }
		
	};
	$scope.scrollBottom = function() {

		if($scope.layout == 'grid'){
			listscroll = $ionicScrollDelegate.$getByHandle('grid');
		} else {
			listscroll = $ionicScrollDelegate.$getByHandle('list');
		}

			listscroll.scrollBy(0,300, true);
		// if(listscroll.getScrollPosition().top < listscroll.getScrollView()['__maxScrollTop']){
		// 	$scope.scrollabletop = true;	
		// } else {
		// 	$scope.scrollablebottom = false;
		// }
	};

	//filter section 
	    $scope.availablitySlider = {};
	    $scope.availablitySlider.availablityrangeValue = 0;
	    $scope.ratingSlider = {};
	    $scope.ratingSlider.ratingrangeValue = 0;
};

Application.Controllers.controller('mainCtrl',['$scope','$ionicModal','$location','$ionicSlideBoxDelegate','Productfactory','_','$http', 'limitToFilter', mainCtrl]);
Application.Controllers.controller('phoneCtrl',['$scope','$ionicModal','$stateParams', '$location','$filter' ,'$timeout','Productfactory', '$ionicScrollDelegate', phoneCtrl]);