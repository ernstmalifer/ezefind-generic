'use strict';

var Application = Application || {};

Application.Controllers = angular.module('application.controllers', ['application.filter']);
Application.Services    = angular.module('application.services',[]);
Application.Filters = angular.module('application.filter', []);


angular.module('application',['ionic','application.controllers','application.filter','application.services', 'ui.bootstrap'])
    .run(['$location', '_', 'Productfactory' ,'$rootScope', function($location, _, Productfactory, $rootScope){
        $rootScope.itemAdded = true;
        Productfactory.getAll();
    }])

  .config(function($stateProvider,$urlRouterProvider){
    
    $stateProvider
        .state('home', {
          url: "/home",
          views: {
            'mainView': {
              templateUrl: 'template/homepage.html',
              controller: 'mainCtrl'
            }
          }
        })
        .state('phone', {
            url: '/phone',
            views: {
                'mainView':{
                    templateUrl: 'template/phone.html',
                    controller: 'phoneCtrl'
                }
            }
        })
        .state('phoneitem', {
            url: '/Item/:phoneid',
            views: {
                'mainView':{
                    templateUrl: 'template/phoneitem.html',
                    controller: 'phoneitemCtrl'
                }
            }
        })
        .state('phonesubCategory', {
            url: '/phone/subcategory/:subtype',
            views: {
                'mainView':{
                    templateUrl: 'template/subphone.html',
                    controller: 'phoneCtrl'
                }
            }
        })
        .state('camera', {
          url: '/camera',
          views: {
            'mainView': {
                templateUrl:'template/phone.html',
                controller: 'phoneCtrl'
            }
          }
        })
        .state('cameraitem', {
          url: '/camera/:cameraid',
          views: {
            'mainView': {
                templateUrl:'template/phoneitem.html',
                controller: 'phoneitemCtrl'
            }
          }
        })
        .state('cameraSubCategory', {
          url: '/camera/subcategory/:subtype',
          views: {
            'mainView':{
                templateUrl: 'template/subphone.html',
                controller: 'phoneCtrl'
            }
          }
        })
        .state('tablet', {
          url: '/tablet',
          views: {
            'mainView': {
                templateUrl:'template/phone.html',
                controller: 'phoneCtrl'
            }
          }
        })
        .state('tabSubCategory', {
          url: '/tablet/subcategory/:subtype',
          views: {
            'mainView': {
                templateUrl:'template/subphone.html',
                controller: 'phoneCtrl'
            }
          }
        })
         .state('tabletitem', {
          url: '/tablet/:tabletid',
          views: {
            'mainView': {
                templateUrl:'template/tabletitem.html',
                controller: 'tabletitemCtrl'
            }
          }
        })
        .state('compare', {
          url: '/compare',
          views: {
            'mainView': {
                templateUrl:'template/compare.html',
                controller: 'compareCtrl'
            }
          }
        })
        .state('wishlist', {
          url: '/wishlist',
          views: {
            'mainView': {
                templateUrl:'template/wishlist.html',
                controller: 'wishlistCtrl'
            }
          }
        })
        .state('search', {
            url: '/search/{searchkeyword}',
            views: {
                'mainView':{
                    templateUrl: 'template/search.html',
                    controller: 'searchCtrl'
                }
            }
        })
        .state('Entertainment', {
            url: '/:categorytype',
            views: {
                'mainView':{
                  templateUrl: 'template/phone.html',
                  controller: 'phoneCtrl'
                }
            }
        });


   $urlRouterProvider.otherwise("/home");

})