'use strict';

var wishlistCtrl = function($scope, $location,$ionicModal,$timeout, Productfactory){
	$scope.wishlistitems = {};
  
 	Productfactory.getWishlistItems(function(data){
 		$scope.wishlistitems = data;
 	});

 	$ionicModal.fromTemplateUrl('template/modal/wishlistmodal.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.wishlistmodal = modal;
	});

	$scope.openWishlistModal = function() {
		$scope.wishlistmodal.show();
	};

	$scope.closeWishlistModal = function() {
		$scope.wishlistmodal.hide();
	};

	$scope.goToPhone = function(phoneid){
 		$location.path('/Item/' + phoneid); 
 	}

 	$scope.removeItemToWishlist = function(item, otheritem, index) {
 		$scope.button = true;
 		$scope.removeitem = 'Are you sure you want to delete "' +otheritem.Name+ '" in Wish List';
 		$scope.openWishlistModal();

 		$scope.alert = function(data){
 			if(data  == 'Yes'){
 				Productfactory.removeItemToWishlist(otheritem, index);
 				Productfactory.getWishlistItems(function(data){
			 		$scope.wishlistitems = data;
					$scope.closeWishlistModal();
			 	});
 			}else{
 				$scope.closeWishlistModal();
 			}
 		}
 	}
};

Application.Controllers.controller('wishlistCtrl',['$scope','$location','$ionicModal','$timeout', 'Productfactory', wishlistCtrl]);