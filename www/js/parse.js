'use strict';

var ParseJsonFactory = function() {
    var parsefactory = {};

    parsefactory.parseJsonAttributes = function (data) {
        return getFlatStructure(data);
    };
 
    function getFlatStructure(hierarchicalData){
        var products = [];
        _.each(hierarchicalData.products, function(product){
            products.push(getFlatProduct(product, hierarchicalData));
        });
        return products;
    }
    function getFlatProduct(product, data){
        var flatProduct = _.pick(product, 'id', 'code', 'name', 'price',  'manufacturer',  'startDate', 'endDate');
        fillAttributes(product, flatProduct, data);
        fillCategories(product, flatProduct, data);
        fillImages(product, flatProduct, data);
        return flatProduct;
    }

    function fillImages(product, flatProduct, data){
        if(product.images){
            var images = [];
            _.each(product.images, function(image){
                var file = _.find(data.files, function(f){
                    return f.id === image.file;
                });
                if(!file){
                    return;
                }
                var path = file.path;
                path = path.replace(/\\/gi, '/');
                if(image.default){
                    flatProduct.imageUrl = path;
                }
                images.push(path);
            });
            flatProduct.images = images;
        }
    }

    function fillCategories(product, flatProduct, data){
        var SubChildCategory = _.find(data.categories, function(category){
            return _.any(category.products, function(productId){
                return productId === product.id;
            });
        });
        if(!SubChildCategory){
            SubChildCategory = _.find(data.categories, function(category){
                return category.id === product.category;
            });
        }
        var ChildCategory = SubChildCategory && SubChildCategory.parent && _.find(data.categories, function(category){
            return category.id === SubChildCategory.parent;
        });

        var ParentCategory = ChildCategory && ChildCategory.parent && _.find(data.categories, function(category){
            return category.id === ChildCategory.parent;
        });

        flatProduct.ParentCategory = ParentCategory && ParentCategory.name;
        flatProduct.ChildCategory = ChildCategory && ChildCategory.name;
        flatProduct.SubChildCategory = SubChildCategory && SubChildCategory.name;
    }

    function fillAttributes(product, flatProduct, data){
        _.each(product.attributes, function(productAttribute){
            var attribute = _.find(data.attributes, function(attribute){
                return attribute.id === productAttribute.attribute;
            });
            if(!attribute){
                return;
            }
            var attributeGroup = _.find(data.attributeGroups, function(group){
                return group.id === attribute.group;
            });
            var attributePlace;
            if(attributeGroup){
                attributePlace = flatProduct[attributeGroup.name] || {};
                flatProduct[attributeGroup.name] = attributePlace;
            } else {
                attributePlace = flatProduct;
            }
            attributePlace[attribute.label] = getAttributeValue(attribute, productAttribute.value);
        });
    }

    function getAttributeValue(attribute, value){
        if(!attribute.values || attribute.values.length === 0){
            return value;
        }
        if(angular.isArray(value)){
            var result = [];
            _.each(value, function(singleValue){
                result.push(extractSingleValue(attribute, singleValue));
            });
            return result;
        } else {
            return extractSingleValue(attribute, value);
        }

        function extractSingleValue(attribute, value){
            var attributeValue = _.find(attribute.values, function (av){
                return av.id === value;
            });
            if(attributeValue){
                return attributeValue.value;
            }
            return value; //customizable dropdown/multiselect
        }
    }
   
    return parsefactory;
};

Application.Services.factory('ParseJsonFactory', [ParseJsonFactory]);