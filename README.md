EzeFind Generic
============

Updated: 07/07/2015

Based on ionic starter app

Requirements:

* nodejs and npm `https://nodejs.org`
* ruby for sass `https://www.ruby-lang.org/en/`
* sass `gem install sass`
* ionic `npm install -g ionic`

How to:

* Clone repository
* `cd` to directory
* run `npm install`
* run `ionic serve`

Edit:

* Edit [`index.html`](www/index.html) title fit for Client
* Edit [`config.scss`](scss/config.scss) or for more options [`ionic.app.scss`](scss/ionic.app.scss) for styles
* Edit assets and images [`www/img/*`](www/img/)
* Edit JSON data for products [`www/data.json`](www/data.json)